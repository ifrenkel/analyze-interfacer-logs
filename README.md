## Analyze interfacer logs

This is a script for parsing gcloud error logs of the
[license-db](https://gitlab.com/gitlab-org/security-products/license-db/) project.

### Generating the logs

You need to be logged into the gcloud account.

```bash
gcloud logging read --format=json --freshness=1d resource.type = "cloud_run_revision" AND resource.labels.service_name = "prod-maven-interfacer-cloud-run" AND resource.labels.location = "us-central1" AND severity>=ERROR > logs.json
```