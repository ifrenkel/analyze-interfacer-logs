# frozen_string_literal: true

require 'json/streamer'

errs = {
  /read: connection reset by peer/ => [],
  /read: connection timed out/ => [],
  /connect: network is unreachable/ => [],
  /stopped after \d+ redirects/ => [],
  /EOF/ => [],
  /unsupported protocol scheme/ => [],
  /unexpected HTTP response code/ => [],
  /no such host/ => [],
  /x509: certificate signed by unknown authority/ => [],
  /x509: certificate has expired/ => [],
  /invalid request format: parse/ => [],
  /tls: internal error/ => [],
  /tls: handshake failure/ => [],
  /server misbehaving/ => [],
  /x509: certificate is valid for/ => [],
  /tls: no renegotiation/ => [],
  /http: no Host in request/ => [],
  /i\/o timeout/ => [],
  /context deadline exceeded/ => [],
  /context canceled/ => [],
  /no route to host/ => [],
  /connection refused/ => [],
  /lame referral/ => [],
  /unknown/ => []
}
network_errors=errs.keys-['unsupported protocol scheme', 'http: no Host in request', 'invalid request format: parse']

nlogs = 0
nerrs = 0
nntwrk_errs=0
n500s=0
Dir.glob('./prod/logs.json').each do |file|
  puts file
  f = File.open(file, 'r')
  streamer = Json::Streamer.parser(file_io: f, chunk_size: 500)
  streamer.get(nesting_level: 1) do |log|
    nlogs += 1
    print "\r#{nlogs}" if nlogs % 100 == 0
    loggederr = log.dig('jsonPayload', 'error')
    if loggederr.nil?
      status =log.dig('httpRequest', 'status')
      if status == 500 || status == 504
        n500s+=1
      else
        puts log
      end
      next
    end

    nerrs += 1

    matched = nil
    errs.each do |err, _|
      if loggederr.match(err)
        matched = err
        break
      end
    end

    if matched.nil?
      errs[errs.keys.last] << loggederr
      next
    end

    nntwrk_errs+=1 if network_errors.include?(matched)

    errs[matched] << loggederr
  end
end

puts "\rnum records examined: #{nlogs}"
puts "num 500s: #{n500s}"
puts "num errors: #{nerrs}"
puts "num network errs: #{nntwrk_errs} (pctg of total: #{(nntwrk_errs/nerrs.to_f)*100})"
errs.sort_by { |_k, v| -v.size }.each do |key, errs|
  puts "  #{key.source}: #{errs.size}"
end
pp errs[errs.keys.last]
